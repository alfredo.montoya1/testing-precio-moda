#!/bin/bash

variable=timestamp

tiempoDeEspera=60 #Tiempo entre toma de métricas.

timestamp () {
    timestamp=$(date +%T)
}

timestamp
echo Inicio de medición | tee -a metrics-endurance.txt

for i in {1..151}
do
timestamp
echo $timestamp | tee -a metrics-endurance.txt

kubectl top pod --namespace="dev-preciomoda" | tee -a metrics-endurance.txt

sleep $tiempoDeEspera
done